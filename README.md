# Brightness control

Brightness control is used to control the brightness of all screens connected to your system.

## Installation
#### **Step 1** : clone the repository

```bash
git clone https://gitlab.com/AbdElraoufSabri/brightness-control.git
```
#### **Step 2** : change directory to the cloned folder

```bash
cd brightness-control
```
#### **Step 3** : make the script executable

```bash
chmod +x brightness-control.sh
```

## Examples
### Change the brightness to **0.9** :
#### Example 1 :
```bash
./brightness-control.sh 0.9
brightness successfully changed to 0.9
```

#### Example 2 :
```bash
./brightness-control.sh
Enter new brightness value:
0.9
brightness successfully changed to 0.9
```

## Dependencies
The script depends on only `xrandr`  support in your system
