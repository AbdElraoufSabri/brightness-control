#!/bin/bash

function print_usage
{
    echo "usage: ./set-brightness.sh <0.0:1.0>"
	echo "example: ./set-brightness.sh 0.6"
}

function error_exit
{
	echo "error: $1" 1>&2
    print_usage
	exit 1
}

if [ $# -gt 1 ]; then
    error_exit "incorrect number of arguments"
fi

VALUE=$1

if [ -z $VALUE ]; then
	echo "Enter new brightness value:"
	read VALUE
fi

lt0=$(echo "$VALUE < 0" | bc)
gt1=$(echo "$VALUE > 1" | bc)
if [ $lt0 = '1' ] || [ $gt1 = '1' ];then
    error_exit "new brightness value needs to be within correct range"
fi

if ! [[ $VALUE =~ ^[+-]?[0-9]+?\.?[0-9]*$ ]]; then
    error_exit "argument must be a number"
fi

xrandr -q | grep " connected" | cut -d ' ' -f 1 | while read display; do
   xrandr --output $display --brightness $VALUE
done

echo "brightness successfully changed to $VALUE"
exit 0
